var fotos_div;
var picasa_div;
var carrousel_div;
var title_div;
var holder ;
var myScroll;

jQuery( document ).ready(function(){
	fotos_div = jQuery("#fotos");
	picasa_div = jQuery("#picasa");
	carrousel_div = jQuery("#content_carrusel");
	title_div = jQuery("#back_albumes");
	holder = jQuery("#fotos");
	//
	jQuery(".album").click(function(e) {
		e.preventDefault();
		var id = jQuery(this).attr("data-idalbum");
		var attr_title = jQuery(this).attr("title");

		load_pics(id, attr_title);
	});
	//
	jQuery(document.body).on('click',".titulo_sinCarrusel",function(e){
		e.preventDefault();
		back_to_main();
	});
	jQuery(document.body).on('click','.titulo_conCarrusel',function(e){
		e.preventDefault();
		back_to_photos();
	});
	//For the slider
	jQuery(document.body).on('click','.foto',function(e){
		e.preventDefault();
	        jQuery("#fotos").css("display", "none");
	        jQuery("#content_carrusel").css("display", "block");
	        getgaleria(jQuery(this).index());
	        jQuery("body").removeClass("body_sinCarrusel").addClass("body_conCarrusel");
	        title_div.removeClass("titulo_sinCarrusel");
	        title_div.addClass("titulo_conCarrusel");
	        title_div.find("a").removeClass("titulo_sinCarrusel").addClass("titulo_conCarrusel");
	});
	//For Loading
	jQuery("#loading").ajaxStart(function(){jQuery(this).show();});
	jQuery("#loading").ajaxStop(function(){jQuery(this).hide();});
	//Carrousel
	//Prev and next
	jQuery(".div_prev, .div_next").click(function(e){
		var pulso = jQuery(this).find(".pulso");
		pasar(pulso);
	});
});

function back_to_photos(){
	picasa_div.hide();
	fotos_div.show();
	carrousel_div.hide();
	jQuery(".titulo_conCarrusel").attr("class","titulo_sinCarrusel");
};
function back_to_main(){
	picasa_div.show();
	fotos_div.hide();
	carrousel_div.hide();
	title_div.html("");
	jQuery("#galeria>li").remove();
};

function load_pics(id, attr_title){	
	
		//Before Sending the request
		picasa_div.hide();
		fotos_div.show();
		carrousel_div.hide();
		jQuery(".foto").remove();
		attr_title = "&lt;&lt;&lt; " + attr_title;
		title_div.html(attr_title);
		
		//Request
		jQuery.ajax({
			type : "GET",
			url : "getImagesByGallery",
			data : {
				galleryId : id,
			},
		}).done(function(response) {
			var items = response.result;		
			if (response.success) {
				//Add all items
				for(var i=0;i<items.length;i++){
					var img = jQuery("<img/>");
					var div = jQuery("<div/>",{
						class: 'foto',
					});
					var item = items[i];
					img.attr("src", item.imagenmediana);
					img.attr("big", item.imagengrande);
					
					var shadows = '<div class="shadow-fbox"></div><div class="shadow-izquierda-fbox"></div><div class="shadow-derecha-fbox"></div>';
					var cont = jQuery("<div/>",{
						class: 'imagen-fbox',
					});
					
					cont.append(img);
					div.append(cont);
					div.append(shadows);
					holder.append(div);
				}
			}
		});
		
}

//Maybe need refactoring ...
function getgaleria(index){
    offset = index-1;
    jQuery("#galeria").html(null);
    jQuery("#title").find("#back_albumes").attr("id", "back_fotos");

    var clase = "oculto";
    jQuery(".foto").each(function(){
      if(offset == jQuery(this).index())
        clase = "visible";
      else
        clase = "oculto";
      jQuery("#galeria").append('<li class="'+clase+'" style="background:url('+jQuery(this).find("img").attr("big")+') no-repeat; background-position: center;" offset="'+jQuery(this).index()+'">&nbsp;</li>');
    });

		var widthPrev=jQuery(".div_prev").width();
		var widthNext=jQuery(".div_next").width();
		var widthActual=jQuery("#content_carrusel").width();
		var widthSubstract=2*Math.max(widthPrev,widthNext); //bug hay veces uno de los botones retorna un pixel menos
		var widthUse=widthActual-widthSubstract;
		var heightUse=jQuery("#carrusel").height();
		jQuery("#carrusel").css("width", widthUse+"px");

    jQuery("#galeria > li").each(function(){
      jQuery(this).css("height", heightUse+"px");
      jQuery(this).css("width", widthUse+"px");
    });

    redimensionar(widthUse);

    var width = jQuery("#galeria").width();
    left_left = offset*widthUse*(-1);
    jQuery("#galeria").css("left", left_left+"px");//posicion de la galeria

    jQuery(".div_prev, .div_next").find(".pulso").css("height", jQuery("#galeria").height()+"px");//dando altura a las botones de pase de imagenes
  }

  function redimensionar(widthUse){
    var width = jQuery("#galeria > li").length*widthUse;
    jQuery("#galeria").css("width", width+"px");//dando ancho a la galeria para que corran las imagenes
  }


  function pasar(pulso){
    if(jQuery(pulso).attr("tipo") == "prev"){
      offset--;
      if(offset < 0)
        offset = jQuery("#galeria > li").length - 1;
    }else{
      offset++;
      if(offset == jQuery("#galeria > li").length)
        offset = 0;
    }
    var width = jQuery("#galeria").width();
    left_left = offset*(width/jQuery("#galeria > li").length)*(-1);
    mostrar();
  }

  function mostrar(){
    jQuery("#galeria").animate({
        left: left_left+"px"
      },
      "fast",
      function(){
        var div = jQuery("#galeria > li.visible");
        jQuery(div).removeClass("visible").addClass("oculto");
        jQuery("#galeria > li").filter("[offset="+offset+"]").addClass("visible").removeClass("oculto");
      }
    );
  }

