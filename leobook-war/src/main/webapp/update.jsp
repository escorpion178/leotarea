<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.tarea.leo.servlet.list" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList"%>
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head lang="en">
    <meta charset="ISO-8859-1">
    <meta name="description" content="Corpress - Modern Corporate Template">
    <meta name="author" content="CreateIT">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <title>Editando amigo - Tarea en html5</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">

</script>

    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">


    <!--[if lt IE 9]>
    <script src="assets/bootstrap/js/html5shiv.min.js"></script>
    <script src="assets/bootstrap/js/respond.min.js"></script>
    <![endif]-->

    <script src="assets/js/modernizr.custom.js"></script>
    <script src="assets/js/device.min.js"></script>
</head>
<body class="header-light navbar-light navbar-fixed withAnimation">

<header>

</header>

<div class="container"></div>

<nav class="navbar yamm" role="navigation">
<div class="container">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header hidden-xs">
    <a class="navbar-brand" href="http://www.cloudware360.com"><img src="http://www.flashpanel.com/wp-content/uploads/2013/01/cloudware360-logo.png" alt="Corpress Logo"/></a>
</div>

<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse no-transition" id="bs-example-navbar-collapse-1">
<a href="#" class="showHeaderSearch hidden-xs"><i class="fa fa-fw fa-search"></i></a>
<ul class="nav navbar-nav navbar-right">
</ul>

</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>

<div class="container"></div>
<div class="header-search" style="display:none;">
    <div class="display-table">
        <div class="table-cell">
            <form class="text-center" role="form">
                <fieldset>

                    <div class="form-group">
                        <button class="header-search-icon" type="submit"><i class="fa fa-fw fa-search"></i></button>
                        <label>Por favor ingresa tu busqueda y presiona “enter”</label>
                        <input type="text" class="form-control" placeholder="Type to search...">
                    </div>

                </fieldset>
                <a href="#" class="showHeaderSearch headerSearchClose"><i class="fa fa-fw fa-times"></i></a>
            </form>
        </div>
    </div>
</div>
<div class="container"></div>

<div id="wrapper">

<!-- VISIBLE COPY OF THE HEADER ONLY IN MOBILE NEEDED FOR THE SIDE MENU EFFECT -->

<div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header visible-xs">
        <a class="navbar-brand" href="#"><img src="http://www.flashpanel.com/wp-content/uploads/2013/01/cloudware360-logo.png" alt="Corpress Logo"/></a>
        <button type="button" class="navbar-toggle" id="sidebar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="#" class="showHeaderSearch"><i class="fa fa-fw fa-search"></i></a>
    </div>
</div>

<!-- END -->

<section class="media-section darkbg html5" data-height="220" data-type="video" data-fallback-image="">
    <div class="video">
        <!-- in order for video to be muted you must add  &amp;api=1&amp;player_id=vimeoplayer1 to the end of the video src
            If you have more than one video, make sure that player_id and id have dif names on each video
    -->
        <video id="video1" muted loop autoplay="autoplay" preload="auto"
               poster=''>
            <source src='http://corpress.html.themeforest.createit.pl/assets/videos/teamvideo.mp4' type='video/mp4' />
            <source src='http://corpress.html.themeforest.createit.pl/assets/videos/teamvideo.webm' type='video/webm' />
        </video>
    </div>

    <div class="display-table">
        <div class="inner bg5">
            <div class="text-center">
                <h2 class="uppercase page-header no-margin">Modifica los datos de tu amigo</h2>
            </div>
        </div>
    </div>
</section>

<section class="section bg1" style="color: #313842">
<div class="container">
    <div class="row">
            <div class="col-sm-6">
                <c:if test="${empty customerList}">
            
                <p>No se detecto usuario a editar</p>
            
                </c:if> 


                <c:forEach items="${customerList}" var="customer">

                <form role="form" method="post" action="update">
                    <input type="hidden" name="originalName" id="originalName" 
            value="${customer.name}" />
                    <div class="form-group form-group-float-label">
                        <input type="text" required class="form-control input-lg" id="name" placeholder="Enter email" name="name" value="${customer.name}" >
                        <label for="exampleInputEmail1">Nombre</label>

                    </div>
                    <div class="form-group form-group-float-label">
                        <input type="email" required class="form-control input-lg" id="email" placeholder="Enter email" name="email" value="${customer.email}" >
                        <label for="exampleInputPassword1">E-mail</label>
                    </div>
                
                    <button type="submit" class="update btn btn-primary btn-lg save" title="Update" value="Update">Actualizar</button>
                </form>

                </c:forEach>



            </div>
        </div>
    </div>
</section>

<section class="call-box bg4">
    <div class="inner">
        <div class="container">
            <div class="row">
                <div class="table-cell col-md-7">
                    <h2 class="uppercase no-margin">
                        Sign up to our newsletter!
                    </h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <div class="table-cell col-md-5">
                    <div class="successMessage alert alert-success" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Gracias!!!
                    </div>
                    <div class="errorMessage alert alert-danger" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Ups! An error occured. Please try again later.
                    </div>

                    <form class="form-inline validateIt" role="form" action="assets/form/send.php"
                          method="post" data-email-subject="Newsletter Form" data-show-errors="true"
                          data-hide-form="true">

                        <div class="input-group input-group-lg form-group-float-label">
                            <input id="newsletter2" type="email" name="field[]" required class="form-control">
                            <label for="newsletter2">Email</label>
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Registrate ahora</button>
                            </span>
                            <input type="hidden" name="msg_subject" value="New Newsletter subscription">
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

<div class="container"></div>

<footer>
    
</footer>

<div class="container"></div>

<div class="post-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                Copyright 2014 © creaciones LEO. All rights reserved - <a href="#" target="_blank">creaciones LEO</a>
            </div>
            <div class="col-sm-6 text-right">
                <ul class="list-inline">
                    <li><div class="text-wrapper"><a href="https://www.facebook.com/createITpl" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook fa-fw"></i></a></div></li>
                    <li><div class="text-wrapper"><a href="https://twitter.com/createitpl" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter fa-fw"></i></a></div></li>
                    <li><div class="text-wrapper"><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google +"><i class="fa fa-google-plus fa-fw"></i></a></div></li>
                    <li><div class="text-wrapper"><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="LinkedIn"><i class="fa fa-linkedin fa-fw"></i></a></div></li>
                    <li><div class="text-wrapper"><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest"><i class="fa fa-pinterest fa-fw"></i></a></div></li>
                    <li><div class="text-wrapper"><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dribble"><i class="fa fa-dribbble fa-fw"></i></a></div></li>
                    <li><div class="text-wrapper"><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tumblr"><i class="fa fa-tumblr fa-fw"></i></a></div></li>
                    <li><div class="text-wrapper"><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Instagram"><i class="fa fa-instagram fa-fw"></i></a></div></li>
                    <li><div class="text-wrapper"><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="Flickr"><i class="fa fa-flickr fa-fw"></i></a></div></li>
                </ul>
            </div>
        </div>
    </div>

</div>
</div>


<!-- scripts bellow -->

<script src="assets/js/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.easing.1.3.js"></script>
<!-- IE -->
<script src="assets/js/modernizr.custom.js"></script>

<!-- JS responsible for hover in touch devices -->
<script src="assets/js/jquery.hoverIntent.js"></script>

<!-- Detects when a element is on wiewport -->
<script src="assets/js/jquery.appear.js"></script>

<!-- parallax effect -->
<script src="assets/js/jquery.stellar.min.js"></script>

<!-- Google Maps -->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="assets/js/gmap3.min.js"></script>

<!-- Flexslider -->
<script src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>

<!-- magnific popup -->
<script src="assets/plugins/magnificpopup/jquery.magnific-popup.min.js"></script>

<!-- Responsible for the sidebar navigation in mobile -->
<script src="assets/js/snap.min.js"></script>

<!-- Contact form validation -->
<script src="assets/form/js/contact-form.js"></script>

<!-- our main JS file -->
<script src="assets/js/main.js"></script>


<!-- end scripts -->
</body>
</html>