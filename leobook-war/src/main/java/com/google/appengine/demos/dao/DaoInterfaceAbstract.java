package com.google.appengine.demos.dao;


import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.QueryResultIterator;


public abstract class DaoInterfaceAbstract<T> {
	
	protected static DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
	public abstract T buildBean(Entity entity);
	public abstract Entity buildEntity(T object);

	Logger logger = Logger.getLogger(DaoInterfaceAbstract.class.getName());
	
	public Key saveBean(T bean) {
		return datastoreService.put(buildEntity(bean));
	}
	
	public Key createKey(Class<T> clasz, Object keyValue) {
		Key key = null;
		if(keyValue instanceof Long) {
			key = KeyFactory.createKey(clasz.getSimpleName(), (Long) keyValue);
		} else {
			key = KeyFactory.createKey(clasz.getSimpleName(), (String) keyValue);
		}
		return key;
	}

	public T getBean(Class<T> clasz, Object keyValue) {
		if (keyValue == null) { return null; }
		try {
			Entity entity = datastoreService.get(createKey(clasz, keyValue));
			return (T) buildBean(entity);
		} catch (EntityNotFoundException e) {
			return null;
		}
	}

	public List<T> getBeanList(Class<T> clasz, Filter filtro, Cursor cursor, Integer limit) {
		return getBeanList(clasz, filtro, cursor, limit, false);
	}
	
	public void deleteBean(Class<T> clasz, Object keyValue) {
		datastoreService.delete(createKey(clasz, keyValue));
	}
	public Query getQuery(Class<T> clasz, Filter filtro, boolean keysOnly) {
		Query q = new Query(clasz.getSimpleName());
		q = filtro != null ? q.setFilter(filtro) : q;
		q = keysOnly ? q.setKeysOnly() : q;
		return q;
	}
	public PreparedQuery getPreparedQuery(Class<T> clasz, Filter filtro, boolean keysOnly) {
		PreparedQuery pq = getPreparedQuery(getQuery(clasz, filtro, keysOnly));
		return pq;
	}
	
	public PreparedQuery getPreparedQuery(Query query) {
		PreparedQuery pq = datastoreService.prepare(query);
		return pq;
	}
	
	public FetchOptions getPageOptions(Cursor startCursor, Integer limit) {
		FetchOptions options = FetchOptions.Builder.withDefaults();
		options = limit != null ? options.limit(limit) : options;
		options = startCursor != null ? options.startCursor(startCursor) : options;
		return options;
	}
	public FetchOptions getPageOptions(Cursor startCursor, Cursor endCursor, Integer limit, Integer chunkSize, Integer offset, Integer prefetchSize) {
		FetchOptions options = FetchOptions.Builder.withDefaults();
		options = limit != null ? options.limit(limit) : options;
		options = startCursor != null ? options.startCursor(startCursor) : options;
		options = chunkSize != null ? options.chunkSize(chunkSize) : options;
		options = endCursor != null ? options.endCursor(endCursor) : options;
		options = offset != null ? options.offset(offset) : options;
		options = prefetchSize != null ? options.prefetchSize(prefetchSize) : options;
		return options;
	}
	
	public List<T> getBeanList(Class<T> clasz, Filter filtro, Cursor cursor, Integer limit, boolean keysOnly) {
		Query q = getQuery(clasz, filtro, keysOnly);
		return getBeanList(q, cursor, limit);
	}
	public List<T> getBeanList(Query query, Cursor cursor, Integer limit) {
		return getBeanList(query, getPageOptions(cursor, limit));
	}
	public List<T> getBeanList(Query query, FetchOptions fetchOptions) {
		PreparedQuery pq = getPreparedQuery(query);
		List<T> list = new ArrayList<T>();
		Iterable<Entity> it = pq.asIterable(fetchOptions);
		for (Entity entity : it) {
			list.add(buildBean(entity));
		}
		return list;
	}
	
	public ListCursor<T> getListCursor(Class<T> clasz, Filter filtro, Cursor cursor, Integer limit, boolean keysOnly) {
		Query query = getQuery(clasz, filtro, keysOnly);
		FetchOptions options = getPageOptions(cursor, limit);
		return getListCursor(query, options);
	}
	public ListCursor<T> getListCursor(Query query, FetchOptions fetchOptions) {
		PreparedQuery pq = getPreparedQuery(query);
		QueryResultIterator<Entity> it = pq.asQueryResultIterator(fetchOptions);
		ListCursor<T> listaCursor = new ListCursor<T>();
		while(it.hasNext()) {
			listaCursor.addBean(buildBean(it.next()));
		}
		listaCursor.setCursor(it.getCursor());
		return listaCursor;
	}

	public List<Key> getKeysList(Class<T> clasz, Filter filtro, Cursor cursor, Integer limit) {
		
		PreparedQuery pq =  getPreparedQuery(clasz, filtro, true);
		FetchOptions options = getPageOptions(cursor, limit);
		Iterable<Entity> it = pq.asIterable(options);
		List<Key> list = new ArrayList<Key>();
		for (Entity entity : it) {
			list.add(entity.getKey());
		}
		return list;
	}
}
