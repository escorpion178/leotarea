var height;
var width;
var heightCard;
var widthCard;
var middleVertical;
var middleHorizontal;

var staff = [];
var currentStaff = [];
var currentBoss;
var currentBossLeft;
var currentBossTop;
var index;
var distance = 100;
var factor = 1.5;
var topStaff = 270;
var three = [];

var control =true;
var backPer;

var blocking =false;

var bossList = {};

function setinitialData(jsonData) {
	
	//staff = [{"namePer":"Manuel Aguirre","idPer":"manuel_aguirre","boss":"marco_barrantes","job":"Asesor Comercial","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/IoolQa2JjDusIzR8aHu-WqZOdqUVsxuWs4Ktz8KimZEaTYa1kI852XlvoyUqmEaYB11ytd0OlqnbWTD1WPg\u003ds100"},{"namePer":"Walter Jabo","idPer":"walter_jabo","boss":"juan_rodriguez","job":"Senior Software Developer","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/9VOTQaNgXHhNtaVfWySmAmSPeyzvRabUS4Vna3zvZBmfE7ID1BYZxWwWLLuxCBv0fdN3-B49bBpPfjlS2VE\u003ds100"},{"namePer":"Vanessa Tandazo","idPer":"vanessa_tandazo","boss":"carlos_cortez","job":"","relPer":"0","profilePer":"","photoUrl":"https://lh3.ggpht.com/kpJslxUAHvCcbIANEi7qs2eR8wq8JqKjSjO5Esd5WXt3QsswQ9Zs-TUJ9MUomcx56nD0XBg5ssSdWUMooA\u003ds100"},{"namePer":"Alessandra Carassas","idPer":"alessandra_carassas","boss":"carlos_cortez","job":"Practicante","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/PbhSwLeXTZoTgnOE3s4UU0Jcr2UCt1QQQ3a7sAnGskf7PES_6QBmo-2kT_g5yOMTYJqkQPmR9cw7JmXiR3E\u003ds100"},{"namePer":"Lucía Lira","idPer":"lucia_lira","boss":"patricia_illingworth","job":"Líder Gestión del Cambio","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/tOQD9h0Q7i_PULPeBalGyzkZTYVnsROf5Kb7TytUXCmzyrXVR3DN-zU3CukTx9AXsBWpHgirhS_yrg54vFo\u003ds100"},{"namePer":"Zig Mandel","idPer":"zig_mandel","boss":"saul_chrem","job":"Director de Tecnología","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/GXHO-FZr2V4EeAir7TW9cNINb4HfY21Dw1319Cc5aIcTN_uwtKvfRSQVcXDnePb4uvy_HCFgyaOh3Y1YrQ\u003ds100"},{"namePer":"Katherine Vergara","idPer":"katherine_vergara","boss":"marco_barrantes","job":"Ejecutiva de Cuenta","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/pqKI7aTvtLLH7upEWzANE7VQiA8G7tkF1D0JiQWsxhmLQ0BxkPeIvSohtOCADN3Qm3SRPw1fzIGmx3PhyKB_\u003ds100"},{"namePer":"Cinthia Carreras","idPer":"cinthia_carreras","boss":"patricia_illingworth","job":"Asistente Administrativo","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/240Ipm8rprDzoejKKBvTv7rbv3WeNxTx0B9ilPL3AkeuriS_8F0w4eQkS4H76049wI2qvKNLBaBhPdMyPQ\u003ds100"},{"namePer":"Janina Trujillo","idPer":"janina_trujillo","boss":"saul_chrem","job":"Jefa","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/jbAd8kz8eJ6lk0NSb0tMW1OQhWLgA33mmFqjO5XnIbpmboU0Urx3XpN-TpuYE_6-TRu08KncNXSP0lSaR-Q\u003ds100"},{"namePer":"Alvaro David Huanca Mamani","idPer":"alvaro_huanca","boss":"juan_rodriguez","job":"Practicante","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/O-gN27LZIfb7lRrnVqDQq7wsUuUdvi7cQA1znHGI38t-fLmUobaL_0APNhOtTMYlgmC9g0FiH_La3j7ETg8\u003ds100"},{"namePer":"Raul Garcia","idPer":"raul_garcia","boss":"carlos_cortez","job":"Jefe de Proyectos","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/AxgCJAUwHeieLYzFg4QQAQl61zu1LprxAO40PEo4iWfr4Gulse1WA5ifHakjUd2FvlpK8gPyQHbzOcXfz73A\u003ds100"},{"namePer":"hansy_schmitt","idPer":"hansy_schmitt","boss":"juan_rodriguez","relPer":"0","profilePer":""},{"namePer":"Rodrigo Oran","idPer":"rodrigo_oran","boss":"saul_chrem","job":"Gerente de Marketing Digital","relPer":"0","profilePer":"","photoUrl":"https://lh3.ggpht.com/y9jToB2Y8gYr2xYpHAjCT0WnujD-PV_4ywg0BkgOgU6TAYRoqSjF2i3BWF2UwvFtCWQR3e3lAvOoPpg5lSc\u003ds100"},{"namePer":"Karem Nishikawa","idPer":"karem_nishikawa","boss":"rodrigo_oran","job":"Analista de Marketing digital","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/2CUAmUyL7JD-KA5BlPt_VUvNSeOTFEi3JUAifqE_qFanigWqhW7aJwVJjkzmXZe9SJt33o178lcIxkhvGVc3\u003ds100"},{"namePer":"Tilsa Taboada","idPer":"tilsa_taboada","boss":"marco_barrantes","job":"Analista Comercial","relPer":"0","profilePer":"","photoUrl":"https://lh3.ggpht.com/O-Eplz1B_99AxH1QU9n_kuS6LpKMDsUXERELKRUbwDhvEGrADiwi1isBF-ZRyCht81vpfPnlwCmviGxleg\u003ds100"},{"namePer":"Miguel Enciso","idPer":"miguel_enciso","boss":"carlos_cortez","job":"Instructor","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/FE0I_EMIVyDk5OXzL1dder-Wk2OFUYCH3VR0RoREVSaD_bjkgyEWCx_I7Jx5IS-hD2lCVaQGfdSDA4W5pA\u003ds100"},{"namePer":"Jackeline Canales","idPer":"jackeline_canales","boss":"patricia_illingworth","job":"Asistente Administrativa Financiera","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/mvmjAheCpvR24-taxOgCQWzqfNRtnFa__2LR-oY98dd8HiyTOpHopgCd97P4rEvTlYc309B5sH2YR-jKAw\u003ds100"},{"namePer":"Patricia Illingworth","idPer":"patricia_illingworth","boss":"saul_chrem","job":"Gerente Regional Administrativo Financiero","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/vLJVPUS5UYl_LHZuBIb5sPNXE_ViIAYSJHnL4r-smYRsgnMMkLXMk4-Ww5Kw5xH8CPSKVL5TeGC08RZ9nqs\u003ds100"},{"namePer":"Lincol Claudio","idPer":"lincol_claudio","boss":"saul_chrem","job":"Gerente de Servicios","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/rPapoz0fJVZpwPDrIIyUJ7dnRsgXM5NVdohZDj3LfAPdyostyscqvteriUqrc4buONEVadgwq5vN7dOVBHQ\u003ds100"},{"namePer":"victor_acosta","idPer":"victor_acosta","boss":"marco_barrantes","relPer":"0","profilePer":""},{"namePer":"Graciela López","idPer":"graciela_lopez","boss":"carlos_cortez","job":"Change Management Lead","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/9xv4-ibARPdldrzNItmHJQ9D1eTuu_ayFl7w45XGeVDpoE8FoYgWfgVyfeprvxuEf7jefD22DJTUdrkb9XM\u003ds100"},{"namePer":"Gabriela Cuestas","idPer":"gabriela_cuestas","boss":"marco_barrantes","job":"Ventas","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/6C4ck1yckxHts9FNoMFALOAwRvWEZwCMIL5CJSrtvA0glVNkdQu_EHz_aw2BL1dUS2ZsvlToEKi0CNI625dI\u003ds100"},{"namePer":"Herbert Romero","idPer":"herbert_romero","boss":"carlos_cortez","job":"Deployment Specialist","relPer":"0","profilePer":""},{"namePer":"Brillet Bayona","idPer":"brillet_bayona","boss":"patricia_illingworth","job":"Asistente Administrativo","relPer":"0","profilePer":"","photoUrl":"https://lh3.ggpht.com/F9wpLrzDuPV-JxMP3zuj4M4k0Qz1CVEj7RjADPQzkrs5C2FTzigzYqEPKhVqQlNphucKMObzym64BOdhj1I\u003ds100"},{"namePer":"Julio Berrospi","idPer":"julio_berrospi","boss":"zig_mandel","job":"Jefe de Proyectos","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/yo7gkt9zPSlhbGZZPRE3G4KBU9GAznTPjS4ob8I76_BsOdE_qgMwk07IwuJAtNwBf7Q2ZnwWpG08-c-FDg\u003ds100"},{"namePer":"Yuli Huangal","idPer":"yuli_huangal","boss":"patricia_illingworth","job":"Asistente Administrativo","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/F0Vl1Hb2RK6WRWgwLP42DORUO2iUbbcPqC6bmgP_RO-KRmBPvYqQENLW-W6zlXaLiUewimGHXZ3Zx1MzEXk\u003ds100"},{"namePer":"Malu Mesia","idPer":"malu_mesia","boss":"marco_barrantes","job":"Ventas","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/o-CH8UexCBm8FqeSlcgsUCphxNWg_daRE9PfE1cF0bCQqtyBhjd8fpSoUFKHF3eYGCFo_7gpjbOcqZhYjJzH\u003ds100"},{"namePer":"Wiston Narro","idPer":"wiston_narro","boss":"carlos_cortez","job":"Instructor","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/ZmlxCsaOHe1VUlUT87i_jfsd1wEQKFORbKixpjyJB0H7y_y15MuYOmqsrxNShdGhYOS7vb7QHm_ysjunew\u003ds100"},{"namePer":"Saul Chrem","idPer":"saul_chrem","boss":"none","job":"Gerente General","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/2c_92CxFCgcUw27OaXngFYRJR2EVFVvI9XjKVOSo-yVEruYD1RfwNsf1OV_e5L64i3hRyN9sV6G6sUUCTwk\u003ds100"},{"namePer":"Joan Vargada","idPer":"joan_vargada","boss":"marco_barrantes","job":"Jefe de sistemas televentas","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/nTV81NeDqPsHFvPjnJfcYll4XZlSRXAY-vf7C7NzgZGTO-04NqSv0BOztlvYbC5SwYgVGjqClbmIsto7jiE\u003ds100"},{"namePer":"Juan José Rodriguez","idPer":"juan_rodriguez","boss":"zig_mandel","job":"Jefe de Proyectos","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/I9IHRo0VVjFfMcAoRHwfl88wjZWzE4z9-79voHT8gzUgYbL8jnBvPy4qlcXL9ce9_pt-RAkUgOZKlv4xhVY\u003ds100"},{"namePer":"Carlos Cortez","idPer":"carlos_cortez","boss":"saul_chrem","job":"Jefe de Operaciones","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/KVv1CYI4pxVfI1lzU93dzCIlLia8oW2R6XZZm53RqI93shtD6UleUZrLnqDC22geyNvYm9zmzVfhQdOGAxY\u003ds100"},{"namePer":"Diego Martinez","idPer":"diego_martinez","boss":"juan_rodriguez","job":"Analista Programador","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/wC7x0bpOqNGDSgUFHTG14uxyAWDGE0-ypMrfDltwD1JjJ2GOXQw9Fm3Q1HTyiu97v5pOtzIs3xd_MJHq8jY\u003ds100"},{"namePer":"Karla Peña","idPer":"karla_pena","boss":"juan_rodriguez","job":"Practicante","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/lPZrhiLHVu_wm9Xu0tl8UlXNhpRfip-0FX1fL640r0Vkqq1LilapsTNRsX0aChuO0dwdF52Jv-Ua-mC1-d4\u003ds100"},{"namePer":"Fernando Alfaro","idPer":"fernando_alfaro","boss":"carlos_cortez","job":"Deployment Specialist","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/1uN61lq81-6GIqGSzlYH1Zl8FoccodeHFZJJ6ubF-Is9HlusNgNu-VReWHSBogaadDWNhpILRiJ8J9k_uass\u003ds100"},{"namePer":"Marco Barrantes","idPer":"marco_barrantes","boss":"saul_chrem","job":"Gerente de Ventas","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/eyGkBbFphsGP1q-M17LBqCEwCdfdztCUtxDz7EIH_Q3Gni0LgnQPVL6j21R6dEUQ2DROVf6fpjM6Ly00_Q\u003ds100"}];
	//console.log(jsonData);
	staff = jsonData;
	for(var i=0; i<staff.length; i++){
		bossList[staff[i].boss]=staff[i].boss;
	}
	
	generateCards();
	
	height = $(".container").height();
	width = $(".container").width();
	heightCard = $(".card").height();
	widthCard = $(".card").width();
	middleHorizontal = (width/2)-(widthCard/2);
	middleVertical = (height/4)-(heightCard/2);

	setLocation("none"); 
}



//jQuery(document).ready(function() {
//
//	staff= [{"namePer":"Manuel Aguirre","idPer":"manuel_aguirre","boss":"marco_barrantes","job":"Asesor Comercial","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/IoolQa2JjDusIzR8aHu-WqZOdqUVsxuWs4Ktz8KimZEaTYa1kI852XlvoyUqmEaYB11ytd0OlqnbWTD1WPg\u003ds100"},{"namePer":"Walter Jabo","idPer":"walter_jabo","boss":"juan_rodriguez","job":"Senior Software Developer","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/9VOTQaNgXHhNtaVfWySmAmSPeyzvRabUS4Vna3zvZBmfE7ID1BYZxWwWLLuxCBv0fdN3-B49bBpPfjlS2VE\u003ds100"},{"namePer":"Vanessa Tandazo","idPer":"vanessa_tandazo","boss":"carlos_cortez","job":"","relPer":"0","profilePer":"","photoUrl":"https://lh3.ggpht.com/kpJslxUAHvCcbIANEi7qs2eR8wq8JqKjSjO5Esd5WXt3QsswQ9Zs-TUJ9MUomcx56nD0XBg5ssSdWUMooA\u003ds100"},{"namePer":"Alessandra Carassas","idPer":"alessandra_carassas","boss":"carlos_cortez","job":"Practicante","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/PbhSwLeXTZoTgnOE3s4UU0Jcr2UCt1QQQ3a7sAnGskf7PES_6QBmo-2kT_g5yOMTYJqkQPmR9cw7JmXiR3E\u003ds100"},{"namePer":"Lucía Lira","idPer":"lucia_lira","boss":"patricia_illingworth","job":"Líder Gestión del Cambio","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/tOQD9h0Q7i_PULPeBalGyzkZTYVnsROf5Kb7TytUXCmzyrXVR3DN-zU3CukTx9AXsBWpHgirhS_yrg54vFo\u003ds100"},{"namePer":"Zig Mandel","idPer":"zig_mandel","boss":"saul_chrem","job":"Director de Tecnología","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/GXHO-FZr2V4EeAir7TW9cNINb4HfY21Dw1319Cc5aIcTN_uwtKvfRSQVcXDnePb4uvy_HCFgyaOh3Y1YrQ\u003ds100"},{"namePer":"Katherine Vergara","idPer":"katherine_vergara","boss":"marco_barrantes","job":"Ejecutiva de Cuenta","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/pqKI7aTvtLLH7upEWzANE7VQiA8G7tkF1D0JiQWsxhmLQ0BxkPeIvSohtOCADN3Qm3SRPw1fzIGmx3PhyKB_\u003ds100"},{"namePer":"Cinthia Carreras","idPer":"cinthia_carreras","boss":"patricia_illingworth","job":"Asistente Administrativo","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/240Ipm8rprDzoejKKBvTv7rbv3WeNxTx0B9ilPL3AkeuriS_8F0w4eQkS4H76049wI2qvKNLBaBhPdMyPQ\u003ds100"},{"namePer":"Janina Trujillo","idPer":"janina_trujillo","boss":"saul_chrem","job":"Jefa","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/jbAd8kz8eJ6lk0NSb0tMW1OQhWLgA33mmFqjO5XnIbpmboU0Urx3XpN-TpuYE_6-TRu08KncNXSP0lSaR-Q\u003ds100"},{"namePer":"Alvaro David Huanca Mamani","idPer":"alvaro_huanca","boss":"juan_rodriguez","job":"Practicante","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/O-gN27LZIfb7lRrnVqDQq7wsUuUdvi7cQA1znHGI38t-fLmUobaL_0APNhOtTMYlgmC9g0FiH_La3j7ETg8\u003ds100"},{"namePer":"Raul Garcia","idPer":"raul_garcia","boss":"carlos_cortez","job":"Jefe de Proyectos","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/AxgCJAUwHeieLYzFg4QQAQl61zu1LprxAO40PEo4iWfr4Gulse1WA5ifHakjUd2FvlpK8gPyQHbzOcXfz73A\u003ds100"},{"namePer":"hansy_schmitt","idPer":"hansy_schmitt","boss":"juan_rodriguez","relPer":"0","profilePer":""},{"namePer":"Rodrigo Oran","idPer":"rodrigo_oran","boss":"saul_chrem","job":"Gerente de Marketing Digital","relPer":"0","profilePer":"","photoUrl":"https://lh3.ggpht.com/y9jToB2Y8gYr2xYpHAjCT0WnujD-PV_4ywg0BkgOgU6TAYRoqSjF2i3BWF2UwvFtCWQR3e3lAvOoPpg5lSc\u003ds100"},{"namePer":"Karem Nishikawa","idPer":"karem_nishikawa","boss":"rodrigo_oran","job":"Analista de Marketing digital","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/2CUAmUyL7JD-KA5BlPt_VUvNSeOTFEi3JUAifqE_qFanigWqhW7aJwVJjkzmXZe9SJt33o178lcIxkhvGVc3\u003ds100"},{"namePer":"Tilsa Taboada","idPer":"tilsa_taboada","boss":"marco_barrantes","job":"Analista Comercial","relPer":"0","profilePer":"","photoUrl":"https://lh3.ggpht.com/O-Eplz1B_99AxH1QU9n_kuS6LpKMDsUXERELKRUbwDhvEGrADiwi1isBF-ZRyCht81vpfPnlwCmviGxleg\u003ds100"},{"namePer":"Miguel Enciso","idPer":"miguel_enciso","boss":"carlos_cortez","job":"Instructor","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/FE0I_EMIVyDk5OXzL1dder-Wk2OFUYCH3VR0RoREVSaD_bjkgyEWCx_I7Jx5IS-hD2lCVaQGfdSDA4W5pA\u003ds100"},{"namePer":"Jackeline Canales","idPer":"jackeline_canales","boss":"patricia_illingworth","job":"Asistente Administrativa Financiera","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/mvmjAheCpvR24-taxOgCQWzqfNRtnFa__2LR-oY98dd8HiyTOpHopgCd97P4rEvTlYc309B5sH2YR-jKAw\u003ds100"},{"namePer":"Patricia Illingworth","idPer":"patricia_illingworth","boss":"saul_chrem","job":"Gerente Regional Administrativo Financiero","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/vLJVPUS5UYl_LHZuBIb5sPNXE_ViIAYSJHnL4r-smYRsgnMMkLXMk4-Ww5Kw5xH8CPSKVL5TeGC08RZ9nqs\u003ds100"},{"namePer":"Lincol Claudio","idPer":"lincol_claudio","boss":"saul_chrem","job":"Gerente de Servicios","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/rPapoz0fJVZpwPDrIIyUJ7dnRsgXM5NVdohZDj3LfAPdyostyscqvteriUqrc4buONEVadgwq5vN7dOVBHQ\u003ds100"},{"namePer":"victor_acosta","idPer":"victor_acosta","boss":"marco_barrantes","relPer":"0","profilePer":""},{"namePer":"Graciela López","idPer":"graciela_lopez","boss":"carlos_cortez","job":"Change Management Lead","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/9xv4-ibARPdldrzNItmHJQ9D1eTuu_ayFl7w45XGeVDpoE8FoYgWfgVyfeprvxuEf7jefD22DJTUdrkb9XM\u003ds100"},{"namePer":"Gabriela Cuestas","idPer":"gabriela_cuestas","boss":"marco_barrantes","job":"Ventas","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/6C4ck1yckxHts9FNoMFALOAwRvWEZwCMIL5CJSrtvA0glVNkdQu_EHz_aw2BL1dUS2ZsvlToEKi0CNI625dI\u003ds100"},{"namePer":"Herbert Romero","idPer":"herbert_romero","boss":"carlos_cortez","job":"Deployment Specialist","relPer":"0","profilePer":""},{"namePer":"Brillet Bayona","idPer":"brillet_bayona","boss":"patricia_illingworth","job":"Asistente Administrativo","relPer":"0","profilePer":"","photoUrl":"https://lh3.ggpht.com/F9wpLrzDuPV-JxMP3zuj4M4k0Qz1CVEj7RjADPQzkrs5C2FTzigzYqEPKhVqQlNphucKMObzym64BOdhj1I\u003ds100"},{"namePer":"Julio Berrospi","idPer":"julio_berrospi","boss":"zig_mandel","job":"Jefe de Proyectos","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/yo7gkt9zPSlhbGZZPRE3G4KBU9GAznTPjS4ob8I76_BsOdE_qgMwk07IwuJAtNwBf7Q2ZnwWpG08-c-FDg\u003ds100"},{"namePer":"Yuli Huangal","idPer":"yuli_huangal","boss":"patricia_illingworth","job":"Asistente Administrativo","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/F0Vl1Hb2RK6WRWgwLP42DORUO2iUbbcPqC6bmgP_RO-KRmBPvYqQENLW-W6zlXaLiUewimGHXZ3Zx1MzEXk\u003ds100"},{"namePer":"Malu Mesia","idPer":"malu_mesia","boss":"marco_barrantes","job":"Ventas","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/o-CH8UexCBm8FqeSlcgsUCphxNWg_daRE9PfE1cF0bCQqtyBhjd8fpSoUFKHF3eYGCFo_7gpjbOcqZhYjJzH\u003ds100"},{"namePer":"Wiston Narro","idPer":"wiston_narro","boss":"carlos_cortez","job":"Instructor","relPer":"0","profilePer":"","photoUrl":"https://lh6.ggpht.com/ZmlxCsaOHe1VUlUT87i_jfsd1wEQKFORbKixpjyJB0H7y_y15MuYOmqsrxNShdGhYOS7vb7QHm_ysjunew\u003ds100"},{"namePer":"Saul Chrem","idPer":"saul_chrem","boss":"none","job":"Gerente General","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/2c_92CxFCgcUw27OaXngFYRJR2EVFVvI9XjKVOSo-yVEruYD1RfwNsf1OV_e5L64i3hRyN9sV6G6sUUCTwk\u003ds100"},{"namePer":"Joan Vargada","idPer":"joan_vargada","boss":"marco_barrantes","job":"Jefe de sistemas televentas","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/nTV81NeDqPsHFvPjnJfcYll4XZlSRXAY-vf7C7NzgZGTO-04NqSv0BOztlvYbC5SwYgVGjqClbmIsto7jiE\u003ds100"},{"namePer":"Juan José Rodriguez","idPer":"juan_rodriguez","boss":"zig_mandel","job":"Jefe de Proyectos","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/I9IHRo0VVjFfMcAoRHwfl88wjZWzE4z9-79voHT8gzUgYbL8jnBvPy4qlcXL9ce9_pt-RAkUgOZKlv4xhVY\u003ds100"},{"namePer":"Carlos Cortez","idPer":"carlos_cortez","boss":"saul_chrem","job":"Jefe de Operaciones","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/KVv1CYI4pxVfI1lzU93dzCIlLia8oW2R6XZZm53RqI93shtD6UleUZrLnqDC22geyNvYm9zmzVfhQdOGAxY\u003ds100"},{"namePer":"Diego Martinez","idPer":"diego_martinez","boss":"juan_rodriguez","job":"Analista Programador","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/wC7x0bpOqNGDSgUFHTG14uxyAWDGE0-ypMrfDltwD1JjJ2GOXQw9Fm3Q1HTyiu97v5pOtzIs3xd_MJHq8jY\u003ds100"},{"namePer":"Karla Peña","idPer":"karla_pena","boss":"juan_rodriguez","job":"Practicante","relPer":"0","profilePer":"","photoUrl":"https://lh5.ggpht.com/lPZrhiLHVu_wm9Xu0tl8UlXNhpRfip-0FX1fL640r0Vkqq1LilapsTNRsX0aChuO0dwdF52Jv-Ua-mC1-d4\u003ds100"},{"namePer":"Fernando Alfaro","idPer":"fernando_alfaro","boss":"carlos_cortez","job":"Deployment Specialist","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/1uN61lq81-6GIqGSzlYH1Zl8FoccodeHFZJJ6ubF-Is9HlusNgNu-VReWHSBogaadDWNhpILRiJ8J9k_uass\u003ds100"},{"namePer":"Marco Barrantes","idPer":"marco_barrantes","boss":"saul_chrem","job":"Gerente de Ventas","relPer":"0","profilePer":"","photoUrl":"https://lh4.ggpht.com/eyGkBbFphsGP1q-M17LBqCEwCdfdztCUtxDz7EIH_Q3Gni0LgnQPVL6j21R6dEUQ2DROVf6fpjM6Ly00_Q\u003ds100"}];
//	
//	//loadData();
//
//	for(var i=0; i<staff.length; i++){
//		bossList[staff[i].boss]=staff[i].boss;
//	}
//
//
//	generateCards();
//	
//	height = $(".container").height();
//	width = $(".container").width();
//	heightCard = $(".card").height();
//	widthCard = $(".card").width();
//	middleHorizontal = (width/2)-(widthCard/2);
//	middleVertical = (height/4)-(heightCard/2);
//
//	setLocation("none"); 
//});

function setLocation(idBoss){

	if(blocking){
		return false;
	}

	var staffTemp = searchStaff(idBoss);
	if(idBoss=="none"){
			setLocation(staffTemp[0].idPer);
			return false;
		}

	
	animating();

	if(staffTemp.length==0){
			
			var ctop = $('#'+idBoss).css("top").substring(0, $('#'+idBoss).css("top").length-2)*1;
			$('#'+idBoss).animate({top: (ctop-10)+'px'},"fast");
			$('#'+idBoss).animate({top: (ctop+10)+'px'},"fast");
			$('#'+idBoss).animate({top: ctop+'px'},"fast");
			return false;
	}

	if(currentBoss=='#'+idBoss){
		if(staff[index].boss=='none'){
			$('#'+idBoss).animate({top:'55px'},"fast");
			$('#'+idBoss).animate({top:'75px'},"fast");
			$('#'+idBoss).animate({top:'65px'},"fast");
			return false;
		}
		control =false;
		backPer = idBoss;
		backStaff('#'+idBoss);
		return false;
	}

	if(control){
		var personTemp = {topP:$('#'+idBoss).css("top"), leftP:$('#'+idBoss).css("left"), indexP: index, idP: idBoss};
		three.push(personTemp);
		var div = '<div class="idThree" id="'+idBoss+'-t" onclick="threeBack('+idBoss+')">'+staff[index].namePer+'</div><span id="'+idBoss+'-s"></span>';
		$(".three").append(div);
	}
	if(control){
		cleanLocation(idBoss);		
	}
	setLocationBoss(idBoss);
	setLocationStaff(staffTemp);
	setLinesx(staffTemp.length);
	setLinesy(staffTemp.length);
	currentStaff=staffTemp;
	control=true;
}

function animating(){
	$("#block-click").css("width","100%");
		setTimeout(function(){
			$("#block-click").css("width","0%");
		},500);	
}

function backStaff(idBoss){
	cleanLocation(idBoss);
	setLocation(staff[index].boss);
}

function cleanLocation(idBoss){

	if(!control){
		$(currentBoss).animate({top: three[three.length-1].topP, left: three[three.length-1].leftP});
		$("#"+three[three.length-1].idP+"-s").remove();
		$("#"+three[three.length-1].idP+"-t").remove();
		three.pop();
	}else {  
		$(currentBoss).animate({top: (middleVertical*-2.3)+'px', opacity: 0});	
	}


	for(var i=0; i<currentStaff.length; i++){
		if(idBoss!=currentStaff[i].idPer){
			$('#'+currentStaff[i].idPer).css("position","fixed");
			$('#'+currentStaff[i].idPer).animate({top: "700px", opacity: 0});
			
		}
	}


}

function setLocationBoss(idBoss){
	currentBoss = '#'+idBoss;
	$(currentBoss).css("position","absolute");
	$(currentBoss).animate({left:'295px', top:'65px', opacity: 1});
}

function setLocationStaff(staffTemp){

	switch (staffTemp.length) {
	    case 1:
	             $('#'+staffTemp[0].idPer).css("position","absolute");
	            $('#'+staffTemp[0].idPer).animate({top: topStaff+"px", opacity: 1});
	            break;
	    case 2: 
	    		$('#'+staffTemp[0].idPer).css("position","absolute");
	            $('#'+staffTemp[1].idPer).css("position","absolute");
	    		if(control){
	    			$('#'+staffTemp[0].idPer).css("left","165px");
	            	$('#'+staffTemp[1].idPer).css("left","410px");
	    		}
	    		$('#'+staffTemp[0].idPer).animate({top: topStaff+"px", opacity: 1});
	            $('#'+staffTemp[1].idPer).animate({top: topStaff+"px", opacity: 1});
	            break;
	    case 3: 
	    		$('#'+staffTemp[0].idPer).css("position","absolute");
	            $('#'+staffTemp[1].idPer).css("position","absolute");
	            $('#'+staffTemp[2].idPer).css("position","absolute");
	    		if(control){
	    			$('#'+staffTemp[0].idPer).css("left","95px");
	            	$('#'+staffTemp[1].idPer).css("left","295px");
	            	$('#'+staffTemp[2].idPer).css("left","490px");
	    		}
	    		$('#'+staffTemp[0].idPer).animate({top: topStaff+"px", opacity: 1});
	            $('#'+staffTemp[1].idPer).animate({top: topStaff+"px", opacity: 1});
	            $('#'+staffTemp[2].idPer).animate({top: topStaff+"px", opacity: 1});
	            break;
	    case 4: 
	    		$('#'+staffTemp[0].idPer).css("position","absolute");
	            $('#'+staffTemp[1].idPer).css("position","absolute");
	            $('#'+staffTemp[2].idPer).css("position","absolute");
	            $('#'+staffTemp[3].idPer).css("position","absolute");
	    		if(control){
	    			$('#'+staffTemp[0].idPer).css("left","75px");
	            	$('#'+staffTemp[1].idPer).css("left","215px");
	            	$('#'+staffTemp[2].idPer).css("left","350px");
	            	$('#'+staffTemp[3].idPer).css("left","490px");
	    		}
	    		$('#'+staffTemp[0].idPer).animate({top: topStaff+"px", opacity: 1});
	            $('#'+staffTemp[1].idPer).animate({top: topStaff+"px", opacity: 1});
	            $('#'+staffTemp[2].idPer).animate({top: topStaff+"px", opacity: 1});
	            $('#'+staffTemp[3].idPer).animate({top: topStaff+"px", opacity: 1});
	            break;
	    
	    default:
	    		var tempDistance = distance;
	    		var tempFactor = factor;
	    		var tempTopStaff = topStaff;
	    		var tempI=0;
	    		if(staffTemp.length>3){
	    			tempDistance=20;
	    			factor = 1.5;
	    		}

	            for(var i=0; i<staffTemp.length; i++){
	            	if(i % 5 == 0 && i!=0){
	            			tempTopStaff+=200;
	            			tempI=0;
	            		}
	            	$('#'+staffTemp[i].idPer).css("position","absolute");
	            	if(control){
	            		$('#'+staffTemp[i].idPer).css("left",(tempDistance+((widthCard*factor)*tempI))+"px");
	            	}
	            	$('#'+staffTemp[i].idPer).animate({top: tempTopStaff+"px", opacity: 1});
	            	tempI++;

					
				}
	    		
				
	 	}
}

function searchStaff(idBoss){
	var staffTemp = [];
	var temp = index;

	for(var i = 0 ; i<staff.length; i++){
		if(staff[i].boss==idBoss){
			staffTemp.push(staff[i]);
		}
		if(staff[i].idPer==idBoss){
			index=i;
		}
	}

	if(staffTemp.length==0){
		index=temp;
	}

	return staffTemp;
}

function loadData(){

	var person = {namePer : "Saul Chrem" , idPer : "chrem", boss  : "none", job : "Gerente General", relPer : "normal", profilePer : "#chrem"};
	var person1 = {namePer : "Zig Mandel" , idPer : "mandel", boss  : "chrem", job : "Gerente de Sistemas", relPer : "normal", profilePer : "#mandel"};
	var person2 = {namePer : "Barrantes" , idPer : "barrantes", boss  : "chrem", job : "Gerente de Ventas", relPer : "normal", profilePer : "#barrantes"};
	var person3 = {namePer : "Oran" , idPer : "oran", boss  : "chrem", job : "Gerente de Marketing", relPer : "normal", profilePer : "#oran"};
	var person4 = {namePer : "Rodriguez" , idPer : "rodriguez", boss  : "chrem", job : "Gerente de Proyectos", relPer : "normal", profilePer : "#rodriguez"};
	var person5 = {namePer : "Martinez" , idPer : "martinez", boss  : "chrem", job : "Desarrollo", relPer : "apoyo", profilePer : "#martinez"};
	var person6 = {namePer : "Jabo" , idPer : "jabo", boss  : "chrem", job : "Desarrollo", relPer : "normal", profilePer : "#jabo"};
	var person7 = {namePer : "Schmitt" , idPer : "schmitt", boss  : "chrem", job : "Desarrollo", relPer : "normal", profilePer : "#schmitt"};

	var person8 = {namePer : "Aguirre" , idPer : "aguirre", boss  : "chrem", job : "Marketing", relPer : "asociado", profilePer : "#aguirre"};
	var person9 = {namePer : "Vargada" , idPer : "vargada", boss  : "chrem", job : "Marketing", relPer : "normal", profilePer : "#vargada"};
	var person10 = {namePer : "Acosta" , idPer : "acosta", boss  : "chrem", job : "Marketing", relPer : "normal", profilePer : "#acosta"};
	var person11 = {namePer : "Cuestas" , idPer : "cuestas", boss  : "chrem", job : "Marketing", relPer : "normal", profilePer : "#cuestas"};
	var person12 = {namePer : "Vergara" , idPer : "vergara", boss  : "chrem", job : "Marketing", relPer : "normal", profilePer : "#vergara"};
	var person13 = {namePer : "Mesia" , idPer : "mesia", boss  : "chrem", job : "Marketing", relPer : "normal", profilePer : "#mesia"};
	var person14 = {namePer : "Taboada" , idPer : "taboada", boss  : "chrem", job : "Marketing", relPer : "normal", profilePer : "#taboada"};


	var person15 = {namePer : "Nishikawa" , idPer : "nishikawa", boss  : "oran", job : "Marketing", relPer : "normal", profilePer : "#nishikawa"};



	staff.push(person);
	staff.push(person1);
	staff.push(person2);
	staff.push(person3);
	staff.push(person4);
	staff.push(person5);
	staff.push(person6);
	staff.push(person7);
	staff.push(person8);
	staff.push(person9);
	staff.push(person10);
	staff.push(person11);
	staff.push(person12);
	staff.push(person13);
	staff.push(person14);
	staff.push(person15);
}

function generateCards(){
	for(var i = 0 ; i<staff.length; i++){

		var backgroundColor = '#2c2c2c';
		var img = 'boss_org';
		var backPhoto = "/elementos/img/defaultUsuario2.jpg";
		var prof = '';
		var idPerTemp = staff[i].idPer.split("@");
		var idBossTemp = staff[i].boss.split("@");
		var correo = staff[i].idPer;
		
		staff[i].idPer = idPerTemp[0].replace(".", "_");
		staff[i].boss = idBossTemp[0].replace(".", "_");
		

		if(staff[i].relPer=='apoyo'){
			backgroundColor = '#FF3D00';
		}else if(staff[i].relPer=='asociado'){
			backgroundColor = '#0079FC';
		}
		
		if(bossList[staff[i].idPer]==undefined){
			img = "user_org";
		}

		if(staff[i].photoUrl!=undefined){
			backPhoto = staff[i].photoUrl;
		}

		if(staff[i].job!=undefined){
			prof=staff[i].job;
		}
		var html='<div class="card" style="background: #fff url('+backPhoto+') no-repeat center; background-size: cover; " onclick="setLocation(this.id)" id="'+staff[i].idPer+'">';
		html+='<a title="Ver Perfil" href="verPerfil?email='+correo+'" class="profile" style="background: url(/elementos/img/'+img+'.png) no-repeat center; background-size: cover;" onmouseover="blocckSetLocation()"  onmouseout="unblockSetLocation()"></a>';
		html+='<div class="info">';
		html+='<div class="name" style="background: '+backgroundColor+';">'+staff[i].namePer+'</div>';
		html+='<div class="job">'+prof+'</div>';
		html+='</div>';
		html+='</div>';

		$(".container").append(html);
	}
}

function setLinesx(size){
	
	switch (size) {
	    case 1:
	            $("#x-line").animate({left: "340px", width: "0px"},200);
	            break;
	    case 2: 
	    		$("#x-line").animate({left: "210px", width:  "250px"},200);
	            break;
	    case 3: 
	    		$("#x-line").animate({left: "140px", width:  "400px"},200);
	            break;
	    case 4: 
	    		$("#x-line").animate({left: "125px", width:  "415px"},200);
	            break;
	    case 5: 
	    		$("#x-line").animate({left: "70px", width:  "540px"},200);
	            break;
	    default:
	    		$("#x-line").animate({left: "5px", width:  "605px"},200);
	 	}
}

function setLinesy(size){
	
	
	$(".y-line").remove();
	$(".y-line-2").remove();
	$(".y-line-3").remove();

	switch (size) {
	    case 1:
	            var div = '<div class="y-line" style="left: 339px;"></div>';
				$(".container").append(div);
	            break;
	    case 2: 
	    		for(var i=0; i<size; i++){
	    			var div = '<div class="y-line" style="left: '+((250*i)+210)+'px;"></div>';
					$(".container").append(div);
				}
	    		break;
	    case 3: 
	    		for(var i=0; i<size; i++){
	    			var div = '<div class="y-line" style="left: '+((200*i)+139)+'px;"></div>';
					$(".container").append(div);
				}
	            break;
	    case 4: 
	    		for(var i=0; i<size; i++){
	    			var div = '<div class="y-line" style="left: '+((138*i)+125)+'px;"></div>';
					$(".container").append(div);
				}
	            break;
	    default:
	    		var j=0;
	    		for(var i=0; i<5 ; i++){
	    			var div = '<div class="y-line" style="left: '+((9+((widthCard*1.5)*i))+60)+'px;"></div>';
					$(".container").append(div);

					if(Math.ceil(size/5)>=2){
						var div3='';
						
						//div3 = '<div class="y-line-3" style="left: '+(5+(135*k))+'px; top: '+(320+(200*i))+'px;" ></div>';
						var div2='';
						if(j<size % 5){//(Math.ceil(size/5)*100)+(100*(Math.ceil(size/5)-2))

							div2 = '<div class="y-line-2" style="left: '+(5+(135*i))+'px; height: '+(70+(Math.ceil(size/5)*100)+(100*(Math.ceil(size/5)-2)))+'px;" ></div>';

							for(var k=0; k<Math.ceil(size/5); k++){
								if(k!=0){
									div3 = '<div class="y-line-3" style="left: '+(5+(135*i))+'px; top: '+(320+(200*k))+'px;" ></div>';
									$(".container").append(div3);	
								}
							}
							
						}else if(Math.ceil(size/5)!=2){
							div2 = '<div class="y-line-2" style="left: '+(5+(135*i))+'px; height: '+(70+((Math.ceil(size/5)-1)*100)+(100*(Math.ceil(size/5)-3)))+'px;" ></div>';
							
							for(var k=0; k<Math.ceil(size/5)-1; k++){
								if(k!=0){
									div3 = '<div class="y-line-3" style="left: '+(5+(135*i))+'px; top: '+(320+(200*k))+'px;" ></div>';
									$(".container").append(div3);	
								}
							}
						}
	    				j++;
						$(".container").append(div2);
						console.log("build");
	    			



	    			}
				}



				console.log("lines: "+(Math.ceil(size/5)));
	    		console.log("columns: "+(size % 5));

	    		
				
	 	}

	 	$(".y-line").animate({height: "20px"},200);
	 	$("#y-line-boss").css("height","0px");
	 	$("#y-line-boss").animate({height: "30px"},200);
}

function threeBack(idThreeBack){

	
	var tt=false;
	try{
		if(idThreeBack.id==staff[three[three.length-2].indexP].idPer){
			tt=true;
		}
	}catch(err){
		tt=false;
	}
	
	if(idThreeBack.id == currentBoss.substring(1)){
		return false;
	}else if(tt){
		setLocation(staff[three[three.length-1].indexP].idPer);
	}else{
		setLocation(idThreeBack.id);
	}

	var iTemp = 0;

	for(var i=0; i<three.length; i++){
		if(three[i].idP==idThreeBack.id){
				iTemp = i;
				break;
		}
		
	}
	for(var i=three.length-1; i>iTemp; i--){
		$("#"+three[i].idP+"-s").remove();
		$("#"+three[i].idP+"-t").remove();
		three.pop();
	}	
}

function blocckSetLocation(){
	blocking = true;
}

function unblockSetLocation(){
	blocking = false;
}
