String.prototype.format = function() {
	var formatted = this;
	for (var i = 0; i < arguments.length; i++) {
		var regexp = new RegExp('\\{' + i + '\\}', 'gi');
		formatted = formatted.replace(regexp, arguments[i]);
	}
	return formatted;
};
// Maybe into a library of is own?slides <--
var slides = {};
var threeshold = 0;
var image_slide = '<li><a href="{3}" target="_blank" style="border-style: none;" ><img src="{2}" title="{1}"  width=580 height=346 /></a></li>';
//var video_slide = '<li><div id="myElement{0}" class="add_more_slides"></div><br><br><br><div class="bx-caption"><span>{1}</span></div></li>';
var video_slide = '<li><div id="myElement{0}" class="add_more_slides"></div><br><br><br><div class="bx-caption"><span>{1}</span></div></li>';
var load_more = '';
var bxSlider;
var slider;
var globalPlayer;
var llave = true;
var posSlider=0;
var counter = 1;// To manage the ids
var playingVideo=false;
jQuery(document).ready(function() {
	appendSlides();
	$('video').bind('contextmenu', function(e) {
	    return false;
	}); 

});


function appendSlides() {
	jQuery.each(slides.list, function(pos, slide) {
		

		if (slide.tipo == "imagen") {
			var nuevaNoticia = '<div class="hideOverflow"><div id="noticia'+counter+'" class="noticia" style="background: #fff url('+slide.urlimagen+') no-repeat center; background-size: contain;">';
			nuevaNoticia+='<a target="_top" href="'+slide.url+'" onmouseover="stopAuto()"  onmouseout="restartAuto()"></a>';
			nuevaNoticia+='<div class="msn">'+slide.titulo+'</div></div></div>';
			$('#sliderNoticias').append(nuevaNoticia);
		}
		
		if (slide.tipo == "video") {
			var code ="avc1,mp4a";
    		var code2 ="vp8,vorbis";
    		var nuevoVideo = '<div class="hideOverflow"><a onmouseover="stopAuto()"  onmouseout="restartAuto()">';
    		nuevoVideo+='<div id="noticia'+counter+'" class="noticia">';
    		nuevoVideo+='<video id="video'+counter+'" style="width:100%; overflow:hidden; margin:0px auto; height: 100%;" poster="'+slide.urlimagen+'" controls preload="none">';
    		nuevoVideo+='<source src="'+slide.urlvideo+'" type="video/mp4; codecs='+code+'"/>';
    		nuevoVideo+='<source src="'+slide.urlvideo+'" type="video/webm; codecs='+code2+'"/></video>';
    		nuevoVideo+='<div class="msn">'+slide.titulo+'</div></div></a></div>';
    		$('#sliderNoticias').append(nuevoVideo);

		}
		
		if (slide.tipo == "web") {
			var target="_top";
			if(slide.external=="si"){
				target="_blank";
			}
			var url = 'href="'+slide.url+'"';
			if(slide.url==""){
				url='';
			}
			var nuevaNoticia = '<div class="hideOverflow"><div id="noticia'+counter+'" class="noticia" style="background: #fff url('+slide.urlimagen+') no-repeat center; background-size: contain;">';
			nuevaNoticia+='<a target="'+target+'" '+url+' onmouseover="stopAuto()"  onmouseout="restartAuto()"></a>';
			nuevaNoticia+='<div class="msn">'+slide.titulo+'</div></div></div>';
			$('#sliderNoticias').append(nuevaNoticia);
		}
		

		counter++;
		threeshold = slides.position;
		
	});

}


function loadMoreSlides(threeshold) {
	jQuery.ajax({
		url : 'getNewsByPosition',
		type : 'GET',
		data : {
			"newsPosition" : threeshold
		},
		dataType : "json",
		success : function(response) {
//			console.log("pido desde "+threeshold);
			slides = response.result;
//			console.log("Llegaron "+slides.list.length);
			if (slides.list.length > 0) {
				appendSlides();
			}
		}
	});
}


$(function(){

	if(posSlider==0){

		posSlider=1;
		$('#noticia'+posSlider).css('left','0px');
		try {
			var video = document.getElementById('video'+posSlider);
			video.addEventListener('ended',function(){
		        video.load();
		    });
		}
		catch(err) {
		    
		}
	}
	

$("#next").click(function() {
		

		try {
    		document.getElementById('video'+posSlider).pause();
		}
		catch(err) {
		    
		}
		
		var n = $( ".noticia" ).length;
		
		$('#noticia'+posSlider).animate({left:'-645px'});
			if(n==posSlider){
			posSlider=0;
		}
		
		posSlider++;
		$('#noticia'+posSlider).css('left','645px');
		$('#noticia'+posSlider).animate({left:'0px'});
		//esconde
		if(posSlider==(counter-2)){
			loadMoreSlides(threeshold);
			
		}
		try {
			var video = document.getElementById('video'+posSlider);
			video.addEventListener('ended',function(){
		        video.load();
		    });
		}
		catch(err) {
		    
		}
		clearInterval(myTimer);
		myTimer = setInterval(myFn, 5000);
		
		return false;
	});

$("#before").click(function() {
	
	try {
		document.getElementById('video'+posSlider).pause();
	}
	catch(err) {
	    
	}
	
	$('#noticia'+posSlider).animate({left:'645px'});
	if(posSlider==1){
		posSlider=(counter);
	}
	posSlider--;
	$('#noticia'+posSlider).css('left','-645px');
	$('#noticia'+posSlider).animate({left:'0px'});
	
	try {
		var video = document.getElementById('video'+posSlider);
		video.addEventListener('ended',function(){
	        video.load();
	    });
	}
	catch(err) {
	    
	}
	clearInterval(myTimer);
	myTimer = setInterval(myFn, 5000);
	return false;
});

//$(".ho").click(function() {
//	
//
//	try {
//		if(playingVideo){
//			document.getElementById('video'+posSlider).pause();
//			playingVideo=false;
//		}else{
//			document.getElementById('video'+posSlider).play();
//			playingVideo=true;
//		}
//		
//	}
//	catch(err) {
//	    
//	}
//	
//	
//	
//	return false;
//});



	
});


function myFn() {

	var msn = "--";
	try {
		var ended = document.getElementById('video'+posSlider).ended;
		var time = document.getElementById('video'+posSlider).currentTime;
//		var paused = document.getElementById('video'+posSlider).paused;
//		if(ended || paused){
		if(ended || time==0){	
			msn = "--";
		}else{
			msn="playing: "+time;
		}
	}
	catch(err) {
	    
	}
	
//	console.log(msn);
	if(msn=='--'){
		var n = $( ".noticia" ).length;
		
		$('#noticia'+posSlider).animate({left:'-645px'});
			if(n==posSlider){
			posSlider=0;
		}
		
		posSlider++;
		$('#noticia'+posSlider).css('left','645px');
		$('#noticia'+posSlider).animate({left:'0px'});
		//esconde
		if(posSlider==(counter-2)){
			loadMoreSlides(threeshold);
			
		}
		
	}
	
	return false;
	

console.log("dota");

  	}

	var myTimer = setInterval(myFn, 5000);
	
	function stopAuto(){
		clearInterval(myTimer);
//		console.log("hover");
//		myTimer = setInterval(myFn, 4000);
	}
	function restartAuto(){
//		clearInterval(myTimer);
		myTimer = setInterval(myFn, 5000);
//		console.log("no hover");
	}
	
	

