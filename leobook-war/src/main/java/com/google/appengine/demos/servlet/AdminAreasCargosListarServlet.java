package com.google.appengine.demos.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.demos.bean.Cargo;
import com.google.appengine.demos.dao.CargosDao;
import com.google.appengine.demos.bean.Area;
import com.google.appengine.demos.dao.AreasDao;




public class AdminAreasCargosListarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(AdminAreasCargosListarServlet.class.getName());

	// lista las areas y cargos
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		List<Area> areas = new AreasDao().getAreas();
		List<Cargo> cargos = new CargosDao().getCargos();
		logger.log(Level.WARNING, "areas.size():" + areas.size());
		req.setAttribute("areas", areas);
		req.setAttribute("cargos", cargos);
		resp.setCharacterEncoding("UTF-8");
		req.getRequestDispatcher("admin_areas_cargos.jsp").forward(req, resp);
	}
}

