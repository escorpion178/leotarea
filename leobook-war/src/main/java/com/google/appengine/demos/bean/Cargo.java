package com.google.appengine.demos.bean;

public class Cargo {

	private Long idCargo;
	private String nombre;
	private Long idArea;
	private String nombreArea;

	public Cargo() {
		this.idCargo = System.currentTimeMillis();
	}

	public Cargo(Long idCargo) {
		this.idCargo = idCargo;
	}

	public Long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getIdArea() {
		return idArea;
	}

	public void setIdArea(Long idArea) {
		this.idArea = idArea;
	}

	public String getNombreArea() {
		return nombreArea;
	}

	public void setNombreArea(String nombreArea) {
		this.nombreArea = nombreArea;
	}

}
