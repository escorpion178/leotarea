package com.google.appengine.demos.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.demos.bean.Cargo;
import com.google.appengine.demos.dao.AreasDao;
import com.google.appengine.demos.dao.CargosDao;
import com.google.appengine.demos.util.AreasCargosUtil;
import com.google.gdata.util.common.base.StringUtil;

public class AdminCargosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(AdminCargosServlet.class.getName());

	CargosDao cargosDao = new CargosDao();
	AreasDao areasDao = new AreasDao();
	
	// elimina un cargo
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String strIdCargo = req.getParameter("idCargo");
		Long idCargo = AreasCargosUtil.getLongId(strIdCargo);
		cargosDao.delete(idCargo);
		resp.sendRedirect("/adminAreasCargosListar");
	}

	// guarda o edita un cargo
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String strIdCargo = req.getParameter("idCargo");
		String nombre = req.getParameter("cargo_nombre");
		String strIdArea = req.getParameter("cargo_idArea");
		nombre = StringUtil.isEmptyOrWhitespace(nombre) ? req.getParameter("nombreCargoEditar") : nombre;

		logger.log(Level.WARNING, "strIdCargo:" + strIdCargo + ", nombre:" + nombre + ", strIdArea:" + strIdArea);

		Long idArea = AreasCargosUtil.getLongId(strIdArea);
		Long idCargo = AreasCargosUtil.getLongId(strIdCargo);
		Cargo cargoExiste = cargosDao.getFirstByNombre(nombre);
		logger.log(Level.WARNING, "cargoExiste:" + cargoExiste + ", nombre:" + (cargoExiste != null ? cargoExiste.getNombre() : "ES NULL"));

		boolean cargoNuevoDiferenteNombre = idCargo == null && cargoExiste == null;
		boolean cargoEdicionExiste = idCargo != null && cargoExiste != null;
		boolean mismoCargo = cargoEdicionExiste && idCargo.longValue() == cargoExiste.getIdCargo().longValue();
		boolean cargoEnDiferenteArea = cargoEdicionExiste && idArea.longValue() != cargoExiste.getIdArea().longValue();

		if (cargoNuevoDiferenteNombre || (cargoEdicionExiste && (mismoCargo || cargoEnDiferenteArea))) {
			Cargo cargo = (idCargo != null ? new Cargo(idCargo) : new Cargo());
			cargo.setNombre(nombre);
			cargo.setIdArea(idArea);
			String nombreArea = areasDao.getAreaById(idArea).getNombre();
			cargo.setNombreArea(nombreArea);
			cargosDao.saveCargo(cargo);
			logger.log(Level.WARNING, "Cargo guardado");
			resp.sendRedirect("/adminAreasCargosListar");
		} else {
			resp.sendRedirect("/adminAreasCargosListar?cargoExiste=1");
		}
	}
}


