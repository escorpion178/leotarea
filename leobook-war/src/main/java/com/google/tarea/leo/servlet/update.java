package com.google.tarea.leo.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.demos.bean.Customer;

/**
 * Servlet implementation class update
 */
public class update extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(update.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
//		.addSort("date", Query.SortDirection.DESCENDING);
		String strIdCargo = request.getParameter("idname");		
		Filter Filtro = new FilterPredicate("name", FilterOperator.EQUAL, strIdCargo);		
		Query query = new Query("Customer").setFilter(Filtro);		
		
		List<Entity> customers = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
		
	    List<Customer> newList = new ArrayList<Customer>();
	    for(Entity entity: customers) {
	    	newList.add(new Customer((String)entity.getProperty("name"), (String)entity.getProperty("email")));
	    }
	    request.setAttribute("customerList", newList);
	    response.setCharacterEncoding("UTF-8");
	    request.getRequestDispatcher("update.jsp").forward(request, response);	
	    
	    
		//response.sendRedirect("update.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		// Se recibe los datos desde update.jsp mediante el request
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		
		// Se recbe tambien mediante request el Id del amigo que se guardo en la variable originalName
		String originalName =  request.getParameter("originalName");
		
		// De la entidad Customer se filtra que sea igual a la id "originalName"
		Filter Filtro = new FilterPredicate("name", FilterOperator.EQUAL, originalName);		
		Query query = new Query("Customer").setFilter(Filtro);
		
		// Se alista el query conteniendo la unica entidad Customer para este caso, y guardandola en una Entidad.
		PreparedQuery pq = datastore.prepare(query);
		Entity customer = pq.asSingleEntity();
		
		// Se guardan los datos del update.jsp en la entidad creada customer, se confirma con el put.
		customer.setProperty("name", name);
		customer.setProperty("email", email);
		//customer.setProperty("date", new Date());
        datastore.put(customer);
     
        //return to list
        response.sendRedirect("/list");
        
	}

}
