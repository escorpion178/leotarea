<%@ page import="com.google.appengine.api.users.User"%>
<%@ page import="com.google.appengine.api.users.UserService"%>
<%@ page import="com.google.appengine.api.users.UserServiceFactory"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<!-- Bootstrap core CSS -->
<link href="elementos/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap theme -->
<link href="elementos/css/bootstrap-theme.min.css" rel="stylesheet">

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<title>Admin de �reas y cargos</title>
<script type="text/javascript">

	function validarArea() {
		var nombre = $("#nombre").val();
		if($.trim(nombre)=="") {
			alert("Ingrese un nombre v�lido.");
			return false;
		}
		return true;
	}

	function editarArea(idArea) {
		var nombre = $("#nombre_" + idArea).text();
		var descripcion = $("#descripcion_" + idArea).text();
// 		console.log("nombre:" + nombre);
// 		console.log("descripcion:" + descripcion);
		$("#idArea").val(idArea);
		$("#nombre").val(nombre);	
		$("#nombreEditar").val(nombre);	
		$("#descripcion").val(descripcion);	
		$("#nombre").attr("disabled", "disabled");
	}
	
	
	function cancelarArea() {
		$("#idArea").val("");
		$("#nombre").val("");
		$("#nombreEditar").val("");
		$("#descripcion").val("");
		$("#nombre").removeAttr("disabled");
	}
	
	function eliminarArea(idArea) {
		var nombre = $("#nombre_" + idArea).text();
		if(confirm("Seguro de eliminar el �rea [" + nombre + "]")) {
			location.href = "/adminAreas?idArea=" + idArea;
		}
	}
	
	
	function validarCargo() {
		var nombre = $("#cargo_nombre").val();
		var area = $("#cargo_idArea").val();
		if($.trim(nombre)==="") {
			alert("Ingrese un nombre v�lido.");
			return false;
		}
		if($.trim(area)==="") {
			alert("Escoja un �rea.");
			return false;
		}
		return true;
	}
	
	function editarCargo(idCargo, idArea) {
		var nombre = $("#nombre_cargo_" + idCargo).text();
// 		console.log("nombre:" + nombre);
// 		console.log("idCargo:" + idCargo);
// 		console.log("idArea:" + idArea);
		$("#idCargo").val(idCargo);
		$("#cargo_idArea").val(idArea);
		$("#cargo_nombre").val(nombre);	
		$("#nombreCargoEditar").val(nombre);		
		$("#cargo_nombre").attr("disabled", "disabled");
	}
	
	function cancelarCargo() {
		$("#idCargo").val("");
		$("#cargo_idArea").val("");
		$("#cargo_nombre").val("");
		$("#nombreCargoEditar").val("");
		$("#cargo_nombre").removeAttr("disabled");
	}
	
	function eliminarCargo(idCargo) {
		var nombre = $("#nombre_cargo_" + idCargo).text();
		if(confirm("Seguro de eliminar el cargo [" + nombre + "]")) {
			location.href = "/adminCargos?idCargo=" + idCargo;
		}
	}
</script>

<style type="text/css">
td{
    padding: 10px;
}

#nombre, #descripcion, #cargo_nombre{
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.428571429;
    color: #555555;
    vertical-align: middle;
    background-color: #ffffff;
    background-image: none;
    border: 1px solid #cccccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
}

.border-botones{
    text-align: center;
    background-image: -webkit-gradient(linear,left 0,left 100%,from(#d9edf7),to(#b9def0));
    background-image: -webkit-linear-gradient(top,#d9edf7 0,#b9def0 100%);
    background-image: -moz-linear-gradient(top,#d9edf7 0,#b9def0 100%);
    background-image: linear-gradient(to bottom,#d9edf7 0,#b9def0 100%);
    background-repeat: repeat-x;
    border-color: #9acfea;
    border-radius: 4px;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffd9edf7',endColorstr='#ffb9def0',GradientType=0);

}
.botones{
    background-image: -webkit-gradient(linear,left 0,left 100%,from(#428bca),to(#2d6ca2));
	background-image: -webkit-linear-gradient(top,#428bca 0,#2d6ca2 100%);
	background-image: -moz-linear-gradient(top,#428bca 0,#2d6ca2 100%);
	background-image: linear-gradient(to bottom,#428bca 0,#2d6ca2 100%);
	background-repeat: repeat-x;
	border-color: #2b669a;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff428bca',endColorstr='#ff2d6ca2',GradientType=0);
	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: normal;
	line-height: 1.428571429;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	cursor: pointer;
	border: 1px solid transparent;
	border-radius: 4px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	-o-user-select: none;
	color: #fff;
	background-color: #428bca;
}
.botones:hover, .botones:focus{
    color: #fff;
	background-color: #3276b1;
	border-color: #285e8e;
}
select{
	width: 100%;
	height: 34px;
	padding: 6px 12px;
	font-size: 14px;
	line-height: 1.428571429;
	color: #555;
	vertical-align: middle;
	background-color: #fff;
	background-image: none;
	border: 1px solid #ccc;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
	box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
	-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

table{
	width: 100%;
}
.linea{
	border-top: 1px solid #DADADA;
	border-bottom: 1px solid #DADADA;
}

</style>

</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/index.html">Panel de Inicio</a>
			</div>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">Intranet &nbsp;&nbsp;</a></li>
					<li class="active"><a href="https://www.google.com/settings/personalinfo">Usuario: leito@cloudware360.com</a></li>
				</ul>
			</div>

		</div>
	</div>
	<br>
	<br>
	<br>
	<div class="container" style="margin-left: -15%; padding-left: 15%">
		<div class="row fluid">
		
			<div class="col-md-6 col-md-offset-3">
			<div class="col-md-11" style="width: 900px;">
			<span style="color: red">
			<c:if test="${param.areaExiste=='1'}">
				Ya existe un �rea con ese nombre.
			</c:if>
			<c:if test="${param.cargoExiste=='1'}">
				Ya existe un cargo con ese nombre.
			</c:if>
			<c:if test="${param.tieneCargos=='1'}">
				El �rea que intenta eliminar tiene cargos.
			</c:if>
			</span>
			<table style="margin-top:15px; width: 100%;">
			<tr>
				<td style="padding: 10px;" valign="top">
					Areas<br />
					
					<form action="/adminAreas" method="post" onsubmit="return validarArea()">
						<input type="hidden" name="idArea" id="idArea" />
						<input type="hidden" name="nombreEditar" id="nombreEditar" />
						<table>
							<tr><td><label for="nombre">Nombre Area:</label></td><td><input type="text" name="nombre" id="nombre"/></td></tr>
							<tr><td><label for="descripcion">Descripci�n:</label></td><td><input type="text" name="descripcion" id="descripcion"/></td></tr>
							<tr><td colspan="2">
							<div class="border-botones">
							<input class="botones" type="submit" value="Guardar" style="margin: 10px 10px 10px 10px;" />
							<input class="botones" type="button" onclick="cancelarArea()" value="Cancelar" style="margin: 10px 10px 10px 10px;"/>
							</div>
							</td></tr>
						</table>
					</form>
					<table>
						<thead><tr>
							<td><b>Area</b></td>
							<td><b>Descripci�n</b></td>
							<td colspan="2"><b>Opciones</b></td>
						</tr></thead>
						<tbody id="bodyAreas">
							<c:if test="${empty areas}">
								<tr>
									<td colspan="4" >No hay �reas</td>
								</tr>
							</c:if>
							<c:forEach items="${areas}" var="area">
								<tr class="linea">
									<td id="nombre_${area.idArea}">${area.nombre}</td>
									<td id="descripcion_${area.idArea}">${area.descripcion}</td>
									<td><button class="botones" value="Editar" onclick="editarArea('${area.idArea}')">Editar</button></td>
									<td><button class="botones" value="Eliminar" onclick="eliminarArea('${area.idArea}')">Eliminar</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</td>
				<td style="padding: 10px; padding-left: 100px" valign="top">
				
					Cargos<br />
					
					<form action="/adminCargos" method="post" onsubmit="return validarCargo()">
						<input type="hidden" name="idCargo" id="idCargo" />
						<input type="hidden" name="nombreCargoEditar" id="nombreCargoEditar" />
						<table>
							<tr><td><label for="cargo_nombre">Nombre Cargo:</label></td><td><input type="text" name="cargo_nombre" id="cargo_nombre"/></td></tr>
							<tr><td><label for="cargo_area">&Aacute;rea:</label></td>
							<td>
								<select name="cargo_idArea" id="cargo_idArea">
									<option value="">-- &Aacute;rea --</option>
									<c:forEach items="${areas}" var="area">
										<option value="${area.idArea}">${area.nombre}</option>
									</c:forEach>
								</select>
							</td></tr>
							<tr><td colspan="2">
							<div class="border-botones">
							<input class="botones" type="submit" value="Guardar" style="margin: 10px 10px 10px 10px;"/>
							<input class="botones" type="button" onclick="cancelarCargo()" value="Cancelar" style="margin: 10px 10px 10px 10px;"/>
							</div>
							</td></tr>
						</table>
					</form>
					<table>
						<thead><tr>
							<td><b>Cargo</b></td>
							<td><b>Area</b></td>
							<td colspan="2"><b>Opciones</b></td>
						</tr></thead>
						<tbody id="bodyAreas">
							<c:if test="${empty cargos}">
								<tr>
									<td colspan="4" >No hay cargos</td>
								</tr>
							</c:if>
							<c:forEach items="${cargos}" var="cargo">
								<tr class="linea">
									<td id="nombre_cargo_${cargo.idCargo}">${cargo.nombre}</td>
									<td id="nombre_area_${cargo.idCargo}">${cargo.nombreArea}</td>
									<td><button class="botones" value="Editar" onclick="editarCargo('${cargo.idCargo}', '${cargo.idArea}')">Editar</button></td>
									<td><button class="botones" value="Eliminar" onclick="eliminarCargo('${cargo.idCargo}')">Eliminar</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				
				</td>

			</tr>
			</table>
			</div>
			</div>
		</div>
	</div>
	
	<!-- Bootstrap core JavaScript
     ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="elementos/js/bootstrap.min.js"></script>
</body>
</html>

