(function($) {

    // ////////MODEL/////////////
    var settings = {
        inputField : "#file"
    }, queueList = [];
    var count = -1, index_list = 0;

    var methods = {
        init : function(options) {
            return this.each(function() {
                settings = $.extend({
                    replace : {
                        regexp : "",
                        newvalue : ""
                    },
                    automatic : true,
                    inputField : "#file",
                    activeUploads : 0,
                    simultaneousUploads : 2,
                    uploadsQueue : [],
                  xhr:[],
                  totalActiveUploads:0,
                    OnQueueCompleted : function() {
                    },
                    OnFileAdded : function(file) {
                    },
                    OnMultipleFileAdded : function() {
                    },
                    OnCompleted : function(responseText) {
                    }
                }, options);
            });
        },
        replace : function(regex, value) {
            settings.replace.regexp = regex;
            settings.replace.newvalue = value;

        },
        options : function(key, value) {
            var option = {};
            option[key] = value;
//            console.log(option);
            settings = $.extend(settings, option);
            return settings;
        },
        

        destroy : function() {
            return this.each(function() {
                var $this = $(this), data = $this.data('uploadlist');
                // Namespacing FTW
                $(window).unbind('.uploadlist');
                data.uploadlist.remove();
                $this.removeData('uploadlist');
            });
        },
        OnMultipleFileAdded : function() {
            settings.OnMultipleFileAdded();
            },
        OnFileAdded : function(file) {
            settings.OnFileAdded(file);
//            console.log("Archivo agregado: " + file);
            var name = file.newname;
            settings.totalActiveUploads++;
            viewModel.model.push({
              isVisible:ko.observable(true),
                responseText : "Pending",
                file_name : name,
                progress : ko.observable(0),
                loaded : ko.observable(0),
                total : ko.observable(0),
                abort:function(place){
                  settings.totalActiveUploads--;
//                  console.log(settings.uploadsQueue.indexOf(file));
                  //queueList.splice(place,1);
                  settings.uploadsQueue[settings.uploadsQueue.indexOf(file)]=null;
//                  console.log(settings.uploadsQueue);
                  this.isVisible(false);
              }
            });
        },
        OnStart : function(filename) {
//            console.log("Upload Start: " + filename);
        },
        OnProgress : function(xhr,total, loaded, index, file_name,file) {
            var obj = {
              isVisible:ko.observable(true),
                responseText : "Subiendo...",
                file_name : file_name + ": ",
                progress : (loaded / total) * 100,
                loaded : getSize(loaded),
                total : getSize(total),
              abort:function(place){
                settings.activeUploads--;
                settings.totalActiveUploads--;
//                console.log("delete onprogress");
                xhr.abort();
                this.isVisible(false);
                //settings.uploadsQueue[file.count]=null;
                 // Check if there are any uploads left in a queue:
                 if (settings.uploadsQueue.length) {
                    methods.uploadFile(settings.uploadsQueue.shift());
                } 
                if (settings.totalActiveUploads === 0) 
                  methods.OnQueueCompleted();
              }
            };
          
          
          
            viewModel.model.replace(viewModel.model()[index], obj);
//            console.log("PROGRESS: " + obj.progress + "|" + loaded + "/" + total+"|"+settings.uploadsQueue.indexOf(file));
        },
        OnCompleted : function(responseText, index) {
//          console.log("settings.activeUploads--: "+settings.activeUploads);
            settings.OnCompleted(responseText);
            var old = viewModel.model()[index];
            var obj = {
              isVisible:ko.observable(false),
                responseText : "Completed",
                file_name : old.file_name,
                progress : old.progress,
                loaded : old.loaded,
                total : old.total,
              abort:function(){
//                console.log("oncompleted delete");
                
                this.isVisible(false);
              }

            };

            viewModel.model.replace(viewModel.model()[index], obj);
          settings.totalActiveUploads--;
            // settings.OnComplete();
            if (settings.totalActiveUploads === 0) 
              methods.OnQueueCompleted();
        },
        OnQueueCompleted : function() {
//            console.log("queue ended!");
            settings.OnQueueCompleted();
          index_list=0;
        },
        start : function() {
//          console.log('start: ' + settings.uploadsQueue);
//          console.log('activeUploads: ' + settings.activeUploads);
//          console.log('totalActiveUploads: ' + settings.totalActiveUploads);
            var index = 0;
            for ( var j = index_list; j < settings.uploadsQueue.length; j++) {
              if (settings.activeUploads === settings.simultaneousUploads) {
                break;
              }
                //var file=settings.uploadsQueue[j];
//                console.log(settings.uploadsQueue);
                var file=settings.uploadsQueue.shift();
//              console.log(settings.uploadsQueue);
                if(file!==null)
                methods.uploadFile(file);              
                index = j + 1;
            }         
            index_list = index;
        },
        uploadFile : function(upload) {  
            
            if (upload===null) {
                    methods.uploadFile(settings.uploadsQueue.shift());
                    return;
                }
            if (upload !== undefined)methods.ajaxUpload(upload);
        },
        ajaxUpload : function(upload) {
                   
            var xhr, formData, prop, data = settings.data, key = settings.key || 'file', index;
            index = upload.count;
//            console.log('Beging upload: ' + upload.newname);
            settings.activeUploads += 1;
            xhr = new window.XMLHttpRequest();
            formData = new window.FormData();
            xhr.open('POST', settings.url);

            // Triggered when upload starts:
            xhr.upload.onloadstart = function() {
                // File size is not reported during start!
//                console.log('Upload started: ' + upload.newname);
                methods.OnStart(upload.newname);
            };

            // Triggered many times during upload:
            xhr.upload.onprogress = function(event) {
                // console.dir(event);
                if (!event.lengthComputable) { return; }

                // Update file size because it might be bigger than reported by
                // the fileSize:
//                console.log("File: " + index);
                methods.OnProgress(xhr,event.total, event.loaded, index, upload.newname,upload);
            };

            // Triggered when upload is completed:
            xhr.onload = function(event) {
//                console.log('Upload completed: ' + upload.newname+"|responseText:"+this.responseText+"|"+event.target.responseText);

                // Reduce number of active uploads:
                settings.activeUploads -= 1;

                methods.OnCompleted(event.target.responseText, index);

                // Check if there are any uploads left in a queue:
                if (settings.uploadsQueue.length) {
                    methods.uploadFile(settings.uploadsQueue.shift());
                }
            };

            // Triggered when upload fails:
            xhr.onerror = function() {
//                console.log('Upload failed: ', upload.newname);
            };

            // Append additional data if provided:
            if (data) {
                for (prop in data) {
                    if (data.hasOwnProperty(prop)) {
//                        console.log('Adding data: ' + prop + ' = ' + data[prop]);
                        formData.append(prop, data[prop]);
                    }
                }
            }

            // Append file data:
            formData.append(key, upload, upload.newname);

            // Initiate upload:
            xhr.send(formData);
          
          return xhr;
        }
    };

    $(settings.inputField).bind("change", function(e) {
        settings.files = e.target.files;
        for ( var i = 0; i < settings.files.length; i++) {
            console.dir(settings.files[i]);
            upload = settings.files[i];
            count++;
            upload.count = count;

            upload.newname = upload.name.replace(settings.replace.regexp, settings.replace.newvalue);
//            console.log(upload.newname);

            methods.OnFileAdded(upload);
            settings.uploadsQueue.push(upload);
        }
        methods.OnMultipleFileAdded();
      //if (settings.automatic) methods.uploadFile(upload);
           // else queueList.push(upload);
    });

    $.fn.uploaderList = function(method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        }
        else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        }
        else {
//            console.log('Method ' + method + ' does not exist on jQuery.uploaderList');
        }

    };
    // ////////MODEL/////////////
    // ////////VIEW-MODEL/////////////
    var viewModel = {

        model : ko.observableArray([])
    };

    // Define a new knockout binding to update any progress bar.
    ko.bindingHandlers.updateProgress = {
        update : function(element, valueAccessor) {
            $(element).progressbar();
            var value = ko.utils.unwrapObservable(valueAccessor());
            $(element).progressbar('option', 'value', value);
        }
    };
    ko.applyBindings(viewModel);
    // ////////VIEW-MODEL/////////////
    function getSize(bytes) {
        var sizes = [ 'n/a', 'bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EiB', 'ZiB', 'YiB' ];
        var i = +Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, i)).toFixed(i ? 1 : 0) + ' ' + sizes[isNaN(bytes) ? 0 : i + 1];
    }

})(jQuery);

