package com.google.appengine.demos.dao;

import java.util.List;

import com.google.appengine.demos.bean.Cargo;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

public class CargosDao extends DaoInterfaceAbstract<Cargo>{

	public List<Cargo> getCargos() {
		return getBeanList(Cargo.class, null, null, null);
	}

	public Long saveCargo(Cargo cargo) {
		return saveBean(cargo).getId();
	}

	public void delete(Long idCargo) {
		deleteBean(Cargo.class, idCargo);
	}

	private Filter getFilterNombre(String nombre) {
		return new FilterPredicate("nombre", FilterOperator.EQUAL, nombre);
	}
	private Filter getFilterIdArea(Long idArea) {
		return new FilterPredicate("idArea", FilterOperator.EQUAL, idArea);
	}
	
	public Cargo getFirstByNombre(String nombre, Long idArea) {
		Filter filtroNombre = getFilterNombre(nombre);
		Filter filtroIdArea = getFilterIdArea(idArea);
		Filter filtroTotal = CompositeFilterOperator.and(filtroNombre, filtroIdArea);
		List<Cargo> cargos = getBeanList(Cargo.class, filtroTotal, null, 1);
		return cargos.isEmpty() ? null : cargos.get(0);
	}

	public Cargo getFirstByNombre(String nombre) {
		Filter filtroNombre = getFilterNombre(nombre);
		List<Cargo> cargos = getBeanList(Cargo.class, filtroNombre, null, 1);
		return cargos.isEmpty() ? null : cargos.get(0);
	}

	public boolean existe(String nombre) {
		Filter filtroNombre = getFilterNombre(nombre);
		List<Cargo> cargos = getBeanList(Cargo.class, filtroNombre, null, 1, true);
		return !cargos.isEmpty();
	}

	public boolean getExistenCargosInIdArea(Long idArea) {
		Filter filtroIdArea = getFilterIdArea(idArea);
		List<Cargo> cargos = getBeanList(Cargo.class, filtroIdArea, null, 1, true);
		return !cargos.isEmpty();
	}

	public void eliminarCargos() {
		List<Key> areaKeys = getKeysList(Cargo.class, null, null, null);
		DatastoreServiceFactory.getDatastoreService().delete(areaKeys);
	}

	@Override
	public Cargo buildBean(Entity entity) {
		Cargo cargo = new Cargo();
		cargo.setIdCargo(entity.getKey().getId());
		cargo.setNombre((String) entity.getProperty("nombre"));
		cargo.setIdArea((Long) entity.getProperty("idArea"));
		cargo.setNombreArea((String) entity.getProperty("nombreArea"));
		return cargo;
	}

	@Override
	public Entity buildEntity(Cargo cargo) {
		Entity entity = new Entity(Cargo.class.getSimpleName(), cargo.getIdCargo());
		entity.setProperty("nombre", cargo.getNombre());
		entity.setProperty("idArea", cargo.getIdArea());
		entity.setProperty("nombreArea", cargo.getNombreArea());
		return entity;
	}
}
