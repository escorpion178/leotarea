package com.google.appengine.demos.util;

import com.google.gdata.util.common.base.StringUtil;

public class AreasCargosUtil {
	public static Long getLongId(String strId) {
		Long longId = null;
		try {
			longId = StringUtil.isEmptyOrWhitespace(strId) ? null : Long.valueOf(strId);
		} catch (NumberFormatException nfe) {}
		return longId;
	}

}