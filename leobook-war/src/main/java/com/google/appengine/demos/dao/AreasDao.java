package com.google.appengine.demos.dao;

import java.util.List;

import com.google.appengine.demos.bean.Area;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

public class AreasDao extends DaoInterfaceAbstract<Area> {

	public List<Area> getAreas() {
		return getBeanList(Area.class, null, null, null);
	}

	public Long saveArea(Area area) {
		return saveBean(area).getId();
	}

	public void delete(Long idArea) {
		deleteBean(Area.class, idArea);
	}

	public Area getAreaById(Long idArea) {
		return getBean(Area.class, idArea);
	}

	public Area getFirstByNombre(String nombre) {
		Filter filtroNombre = new FilterPredicate("nombre", FilterOperator.EQUAL, nombre);
		List<Area> areas = getBeanList(Area.class, filtroNombre, null, 1);
		return areas.isEmpty() ? null : areas.get(0);
	}

	public boolean existe(String nombre) {
		return getFirstByNombre(nombre) != null;
	}

	public void eliminiarAreas() {
		List<Key> areaKeys = getKeysList(Area.class, null, null, null);
		DatastoreServiceFactory.getDatastoreService().delete(areaKeys);
	}

	@Override
	public Area buildBean(Entity entity) {
		Area area = new Area();
		area.setIdArea(entity.getKey().getId());
		area.setNombre((String) entity.getProperty("nombre"));
		area.setDescripcion((String) entity.getProperty("descripcion"));
		return area;
	}

	@Override
	public Entity buildEntity(Area area) {
		Entity entity = new Entity(Area.class.getSimpleName(), area.getIdArea());
		entity.setProperty("nombre", area.getNombre());
		entity.setUnindexedProperty("descripcion", area.getDescripcion());
		return entity;
	}
}
