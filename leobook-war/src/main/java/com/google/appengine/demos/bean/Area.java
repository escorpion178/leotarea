package com.google.appengine.demos.bean;


public class Area {
	private Long idArea;
	private String nombre;
	private String descripcion;

	public Area() {
		this.idArea = System.currentTimeMillis();
	}

	public Area(Long idArea) {
		this.idArea = idArea;
	}

	public Long getIdArea() {
		return idArea;
	}

	public void setIdArea(Long idArea) {
		this.idArea = idArea;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
