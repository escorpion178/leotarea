package com.google.appengine.demos.dao;

import java.util.ArrayList;
import java.util.List;

import com.google.appengine.api.datastore.Cursor;

public class ListCursor<T> {

	private List<T> lista = new ArrayList<T>();
	private Cursor cursor;
	
	public ListCursor() {
	}

	public List<T> getList() {
		return lista;
	}
	
	public void addBean(T bean) {
		this.lista.add(bean);
	}

	public Cursor getCursor() {
		return cursor;
	}

	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
	}
}