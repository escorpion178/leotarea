package com.google.appengine.demos.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.demos.bean.Area;
import com.google.appengine.demos.dao.AreasDao;
import com.google.appengine.demos.dao.CargosDao;
import com.google.appengine.demos.util.AreasCargosUtil;
import com.google.gdata.util.common.base.StringUtil;

public class AdminAreasServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(AdminAreasServlet.class.getName());

	CargosDao cargosDao = new CargosDao();
	AreasDao areasDao = new AreasDao();
	
	// elimina un area
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String strIdArea = req.getParameter("idArea");
		Long idArea = AreasCargosUtil.getLongId(strIdArea);
		//boolean tieneCargos = cargosDao.getExistenCargosInIdArea(idArea);
		//logger.log(Level.WARNING, "tieneCargos:" + tieneCargos);
		//if (tieneCargos) {
			//resp.sendRedirect("/adminAreasCargosListar?tieneCargos=1");
		//} else {
			cargosDao.delete(idArea);
			resp.sendRedirect("/adminAreasCargosListar");
		//}
	}
	

		
		

	// Guarda las areas nuevas y editadas.
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String strIdArea = req.getParameter("idArea");
		String nombre = req.getParameter("nombre");
		nombre = StringUtil.isEmptyOrWhitespace(nombre) ? req.getParameter("nombreEditar") : nombre;
		String descripcion = req.getParameter("descripcion");

		Long idArea = AreasCargosUtil.getLongId(strIdArea);
		Area areaExiste = areasDao.getFirstByNombre(nombre);

		if ((idArea == null && areaExiste == null) || (idArea != null && areaExiste != null && idArea.longValue() == areaExiste.getIdArea().longValue())) {
			Area area = idArea != null ? new Area(idArea) : new Area();
			area.setNombre(nombre);
			area.setDescripcion(descripcion);
			areasDao.saveArea(area);
			logger.log(Level.WARNING, "Area guardada");
			resp.sendRedirect("/adminAreasCargosListar");
		} else {
			resp.sendRedirect("/adminAreasCargosListar?areaExiste=1");
		}
	}

}