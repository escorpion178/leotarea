
var marginCumple=14;
var movCumple=50;
var marginEvento=14;
var movEvento=50;

$(function(){

	$("#before-cumple").click(function(){
		if(marginCumple!=14){
			marginCumple+=movCumple;
			$('#ul-cumple').animate({marginTop:marginCumple});
		}
	});
	
	$("#next-cumple").click(function(){
		var numItems = $('.li-calendar-cumple').length;
		if(numItems!=1 && numItems!=2 && numItems!=3){
			if(marginCumple!=(14-((numItems-3)*50))){
				marginCumple-=movCumple;
				$('#ul-cumple').animate({marginTop:marginCumple});
			}
		}
		
	});

	$("#before-event").click(function(){
		if(marginEvento!=14){
			marginEvento+=movEvento;
			$('#ul-evento').animate({marginTop:marginEvento});
		}
	});
	
	$("#next-event").click(function(){
		var numItems = $('.li-calendar-evento').length;
		if(numItems!=1 && numItems!=2 && numItems!=3){
			if(marginEvento!=(14-((numItems-3)*50))){
				marginEvento-=movEvento;
				$('#ul-evento').animate({marginTop:marginEvento});
			}
		}
		
	});
	
	
});